<!DOCTYPE html>
<html>
<head>
   <title></title>
</head>
<body>
   <table width="70%" border="1px" align="center">
   <tr>
      <td><b>id_persona</b></td>
      <td><b>dni</b></td>
      <td><b>apyn</b></td>
      <td><b>sexo</b></td>
      <td><b>genero</b></td>
      <td><b>direccion</b></td>
      <td><b>barrio</b></td>
      <td><b>calle</b></td>
      <td><b>altura</b></td>
      <td><b>depto</b></td>
      <td><b>control</b></td>
      

   </tr>
<?php

include 'data_cleaner.php';
include 'query_base_pro.php';

$cantidad =0;

if(!connection181()) {
  echo "Error : No se pudo establecer la conexion a la base de datos: ";
  
}else {

    
      //$personas = filtro_sube200_cidig_200();
        //$personas = filtro_territorio_cid();
        //$personas = filtro_sac_personas();
      
      if(!empty($personas)){
        while ($persona = pg_fetch_array($personas)) {

          // $id_persona = ultimoReg_idPersona();
          // $id_p = pg_fetch_array($id_persona);
          // $count_persona = $id_p['ult_idper'] + 1;

          $dni_persona = $persona['dni'];
          $apyn = pg_escape_string($persona['apyn']);
          $barrio = $persona['id_barrio'];
          $genero = $persona['genero'];
          $fecha_nac = $persona['fecha_nac'];
          $email = pg_escape_string($persona['email']);
          $calle = $persona['id_calle'];
          $altura = $persona['altura'];
          $depto = pg_escape_string($persona['dpto']);
          $otro_dato_domi = pg_escape_string($persona['observaciones']);
          $nivel_educ = $persona['id_nivel_educacion'];
          $est_civil = pg_escape_string($persona['est_civil']);
        
          // $apellido = pg_escape_string($persona['apellido']);
          // $nombres = pg_escape_string($persona['nombres']);

          // $apyn = strtoupper($apellido) . ', ' . ucwords(strtolower($nombres));

          
          $apellido = strstr($apyn, ',', true);
          $nombres = strstr($apyn, ',');
          $apeynomMay = strtoupper($apellido).ucwords(strtolower($nombres));
          if (!empty($apeynomMay)){
            $apeynom = $apeynomMay;
          }else {
            $apellido = strstr($apyn, ' ', true);
            $nombres = strstr($apyn, ' ');
            $apeynom = strtoupper($apellido) . ',' . ucwords(strtolower($nombres));
          }
          //$apeynom = ucwords(strtolower($apyn));



          $gen = $persona['genero'];
          $genero = substr($gen, 0,1);
          // $direccion = pg_escape_string($persona['direccion']); 
          // $barrio = pg_escape_string($persona['barrio']);

          //tipo DNI
          $id_tipo_documento = 2;
          //por defecto Argentina
          $id_nacionalidad =1;

          $control_doc =$dni_persona.$id_tipo_documento.$id_nacionalidad; 
          $sex = $persona['sexo'];
          $sexo = substr($sex, 0,1);

          if($sexo == 'V'){
            $id_sexo = 1;
          }else if ($sexo == 'M'){
            $id_sexo = 2;
          }else {
            $id_sexo =NULL;
          }


          if($genero == 'M'){
            $id_genero = 1;
          }else if ($genero == 'F'){
            $id_genero = 2;
          }else {
            $id_genero=NULL;
          }

          $insert_persona = insert_territorioPersonas_cidig181($dni_persona,$email,$id_tipo_documento,$control_doc);

          if($insert_persona){
            $id_persona = get_idPersona($dni_persona);
            $id_p = pg_fetch_array($id_persona);
            $idPersona = $id_p['id_pers'];
            //inserto Persona Natural
            $insertar_personaNat = insert_territorioPersonas_natural($idPersona,$apeynom,$fecha_nac,$id_sexo,$id_genero);

            if($insertar_personaNat){
              //inserto domicilio
                $insertar_dom = insert_domicilio_cidig($barrio,$calle,$altura,$depto,$otro_dato_domi);

                if($insertar_dom){

                  $id_dom = get_idDomicilio181();
                  $id_d = pg_fetch_array($id_dom);
                  $idDomicilio = $id_d['id_dom'];
                  //inserto persona_domicilio 
                  $insertar_per_dom = insert_persona_domicilio_cidig181($idPersona, $idDomicilio);
                }
            }
          }
          
          
$cantidad +=1;
?>
            <tr>
               <td><?php echo $cantidad?></td>
               <td><?php echo $dni_persona?></td>
               <td><?php echo $apeynom?></td>
               <td><?php echo $id_sexo . '->' .$sexo?></td>
               <td><?php echo $id_genero . '->' .$genero?></td>
               <td><?php echo $direccion?></td>
               <td><?php echo $barrio?></td>
               <td><?php echo $calle?></td>
               <td><?php echo $altura?></td>
               <td><?php echo $depto?></td>
               <td><?php echo $control_doc?></td>
            </tr>
         
<?php 

      }
    }
  }

//cerrar conexion a la BD
pg_close(connection181());

   
?>

</table>
</body>
</html>